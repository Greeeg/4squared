EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:4Squared
LIBS:CameraTrigger
LIBS:node
LIBS:4Squared-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SW_PUSH_SMALL SW9
U 1 1 55521BE2
P 3250 2050
F 0 "SW9" H 3400 2160 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 3250 1971 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 3250 2050 60  0001 C CNN
F 3 "" H 3250 2050 60  0000 C CNN
	1    3250 2050
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW10
U 1 1 55521F8F
P 3250 2550
F 0 "SW10" H 3400 2660 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 3250 2471 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 3250 2550 60  0001 C CNN
F 3 "" H 3250 2550 60  0000 C CNN
	1    3250 2550
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW11
U 1 1 555221CD
P 3250 3050
F 0 "SW11" H 3400 3160 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 3250 2971 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 3250 3050 60  0001 C CNN
F 3 "" H 3250 3050 60  0000 C CNN
	1    3250 3050
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW12
U 1 1 555221DB
P 3250 3550
F 0 "SW12" H 3400 3660 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 3250 3471 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 3250 3550 60  0001 C CNN
F 3 "" H 3250 3550 60  0000 C CNN
	1    3250 3550
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW13
U 1 1 55524C48
P 3750 2050
F 0 "SW13" H 3900 2160 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 3750 1971 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 3750 2050 60  0001 C CNN
F 3 "" H 3750 2050 60  0000 C CNN
	1    3750 2050
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW14
U 1 1 55524C4E
P 3750 2550
F 0 "SW14" H 3900 2660 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 3750 2471 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 3750 2550 60  0001 C CNN
F 3 "" H 3750 2550 60  0000 C CNN
	1    3750 2550
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW15
U 1 1 55524C54
P 3750 3050
F 0 "SW15" H 3900 3160 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 3750 2971 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 3750 3050 60  0001 C CNN
F 3 "" H 3750 3050 60  0000 C CNN
	1    3750 3050
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW16
U 1 1 55524C5A
P 3750 3550
F 0 "SW16" H 3900 3660 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 3750 3471 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 3750 3550 60  0001 C CNN
F 3 "" H 3750 3550 60  0000 C CNN
	1    3750 3550
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW1
U 1 1 55524DD8
P 2250 2050
F 0 "SW1" H 2400 2160 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 2250 1971 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 2250 2050 60  0001 C CNN
F 3 "" H 2250 2050 60  0000 C CNN
	1    2250 2050
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW2
U 1 1 55524DDE
P 2250 2550
F 0 "SW2" H 2400 2660 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 2250 2471 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 2250 2550 60  0001 C CNN
F 3 "" H 2250 2550 60  0000 C CNN
	1    2250 2550
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW3
U 1 1 55524DE4
P 2250 3050
F 0 "SW3" H 2400 3160 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 2250 2971 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 2250 3050 60  0001 C CNN
F 3 "" H 2250 3050 60  0000 C CNN
	1    2250 3050
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW4
U 1 1 55524DEA
P 2250 3550
F 0 "SW4" H 2400 3660 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 2250 3471 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 2250 3550 60  0001 C CNN
F 3 "" H 2250 3550 60  0000 C CNN
	1    2250 3550
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW5
U 1 1 55524DF0
P 2750 2050
F 0 "SW5" H 2900 2160 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 2750 1971 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 2750 2050 60  0001 C CNN
F 3 "" H 2750 2050 60  0000 C CNN
	1    2750 2050
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW6
U 1 1 55524DF6
P 2750 2550
F 0 "SW6" H 2900 2660 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 2750 2471 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 2750 2550 60  0001 C CNN
F 3 "" H 2750 2550 60  0000 C CNN
	1    2750 2550
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW7
U 1 1 55524DFC
P 2750 3050
F 0 "SW7" H 2900 3160 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 2750 2971 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 2750 3050 60  0001 C CNN
F 3 "" H 2750 3050 60  0000 C CNN
	1    2750 3050
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL SW8
U 1 1 55524E02
P 2750 3550
F 0 "SW8" H 2900 3660 30  0000 C CNN
F 1 "SW_PUSH_SMALL" H 2750 3471 30  0000 C CNN
F 2 "4Squared:Silicone_button" H 2750 3550 60  0001 C CNN
F 3 "" H 2750 3550 60  0000 C CNN
	1    2750 3550
	1    0    0    -1  
$EndComp
Text HLabel 1250 2100 0    60   Input ~ 0
B[1...16]
Entry Wire Line
	2000 1850 2100 1950
Entry Wire Line
	2550 1850 2650 1950
Entry Wire Line
	3050 1850 3150 1950
Entry Wire Line
	3550 1850 3650 1950
Entry Wire Line
	2050 2350 2150 2450
Entry Wire Line
	2550 2350 2650 2450
Entry Wire Line
	3050 2350 3150 2450
Entry Wire Line
	3550 2350 3650 2450
Entry Wire Line
	3550 2850 3650 2950
Entry Wire Line
	3550 3350 3650 3450
Entry Wire Line
	3050 3350 3150 3450
Entry Wire Line
	2550 3350 2650 3450
Entry Wire Line
	3050 2850 3150 2950
Entry Wire Line
	2550 2850 2650 2950
Entry Wire Line
	2050 2850 2150 2950
Entry Wire Line
	2050 3350 2150 3450
Wire Bus Line
	1800 1850 3550 1850
Wire Bus Line
	1800 2350 3550 2350
Wire Bus Line
	1800 2850 3550 2850
Wire Bus Line
	1800 3350 3550 3350
Wire Wire Line
	2350 2150 2350 3850
Wire Wire Line
	2850 2150 2850 3850
Wire Wire Line
	3350 2150 3350 3850
Wire Wire Line
	3850 3850 3850 2150
Wire Wire Line
	2350 3850 3850 3850
Wire Wire Line
	3100 3850 3100 4200
Connection ~ 3100 3850
Connection ~ 3350 3850
Connection ~ 2850 3850
Connection ~ 2350 3650
Connection ~ 2850 3650
Connection ~ 3350 3650
Connection ~ 2350 3150
Connection ~ 2350 2650
Connection ~ 2850 2650
Connection ~ 3350 2650
Connection ~ 3850 2650
Connection ~ 3850 3150
Connection ~ 3850 3650
Connection ~ 3350 3150
Connection ~ 2850 3150
Text Label 2150 1950 0    60   ~ 0
S_1
Text Label 2650 1950 0    60   ~ 0
S_2
Text Label 3150 1950 0    60   ~ 0
S_3
Text Label 3650 1950 0    60   ~ 0
S_4
Text Label 2150 2450 0    60   ~ 0
S_5
Text Label 2650 2450 0    60   ~ 0
S_6
Text Label 3150 2450 0    60   ~ 0
S_7
Text Label 3650 2450 0    60   ~ 0
S_8
Text Label 2150 2950 0    60   ~ 0
S_9
Text Label 2650 2950 0    60   ~ 0
S_10
Text Label 3150 2950 0    60   ~ 0
S_11
Text Label 3650 2950 0    60   ~ 0
S_12
Text Label 2150 3450 0    60   ~ 0
S_13
Text Label 2650 3450 0    60   ~ 0
S_14
Text Label 3150 3450 0    60   ~ 0
S_15
Text Label 3650 3450 0    60   ~ 0
S_16
Wire Wire Line
	2100 1950 2150 1950
Text Label 2850 1850 0    60   ~ 0
S_[1...16]
Wire Bus Line
	1800 1850 1800 2350
Wire Bus Line
	3550 2850 3550 2350
Wire Bus Line
	1800 2850 1800 3350
Wire Bus Line
	1250 2100 1700 2100
Text Label 1300 2100 0    60   ~ 0
S_[1...16]
$Comp
L GND #PWR?
U 1 1 557E4EF9
P 3100 4200
F 0 "#PWR?" H 3100 3950 50  0001 C CNN
F 1 "GND" H 3100 4050 50  0000 C CNN
F 2 "" H 3100 4200 60  0000 C CNN
F 3 "" H 3100 4200 60  0000 C CNN
	1    3100 4200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
