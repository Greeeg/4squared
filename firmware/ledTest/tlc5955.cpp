#include <msp430.h>
#include <stdbool.h>
#include "tlc5955.h"

extern "C" {

/* P3 */
#define LAT BIT3
#define SCLK BIT2
#define SOUT BIT0
#define SIN BIT1

/* P6 */
#define CLK_EN BIT0

/* P4 */
#define RX BIT5
#define TX BIT4

/* buttons 
P1 and P2 */


/* static 48 int array, 768 bits */
unsigned int tlc_dma_array[48];


void dma_init( void );

void xmit_byte( unsigned int d )
{
	unsigned int i = 0;
	for(i = 0 ; i < 16; i++)
	{
		/* bit by bit */
		if(d & 0x8000)
			P3OUT |= SOUT;
		else
			P3OUT &= ~SOUT;
		d <<= 1;
		P3OUT |= SCLK;
		P3OUT &= ~(SCLK);
	}
}

/* Transfer 96 bytes, 768 bits */
void tlc5955_xmit_common( void )
{
	/* simple implementation */
	unsigned int i = 0;
	for( i = 0 ; i < 48; i++ )
	{
		xmit_byte( tlc_dma_array[47 - i] );
	}
}

/* transfer latch select bit */
void tlc5955_xmit_latch_bit( bool on )
{
	/* single bit */
	if( on )
		P3OUT |= SOUT;
	else
		P3OUT &= ~SOUT;
	P3OUT |= SCLK;
	P3OUT &= ~SCLK;
}

/* insert bit stream segment, of [width] from [input_data] into main bitstream [array] at [offset] */
void tlc5955_byte_insertA( unsigned int* array, unsigned int width, unsigned int input_data, unsigned int bit_offset )
{
	unsigned int mask = 0xFFFF;
	mask = mask >> (16 - width);

	unsigned word_offset = bit_offset / 16;
	bit_offset %= 16;

	mask <<= bit_offset;

	array[word_offset] &= ~mask;
	array[word_offset] |= mask & (input_data << bit_offset);

	if(bit_offset + width > 15) /* need to deal with alignment over 2 words */
	{
		unsigned int extra = (bit_offset + width) % 16;
		word_offset++;

		mask = 0xFFFF >> (16 - extra);

		/* shift bits opposite direction */
		array[word_offset] &= ~mask;
		array[word_offset] |= mask & (input_data >> (16 - bit_offset)); 
	}
}

void tlc5955_byte_insert( unsigned int* array, unsigned int width, unsigned int input_data, unsigned int bit_offset )
{
	unsigned int bit = 0;
	while(width)
	{

	bool bit_on = input_data & 1;
	input_data >>= 1;
	width--;

	unsigned int word_offset = (bit_offset) / 16;
	unsigned int bit_num = (bit_offset++) % 16;

	if(bit_on)
		array[word_offset] |= (1 << bit_num);

	}

}

/* initilise MSP430 end of communications */
void tlc5955_port_init( void )
{
	P3DIR |= (SOUT | SCLK | LAT);
	P3OUT &= ~(SOUT | SCLK | LAT);
	
	P6DIR |= CLK_EN;
	P6OUT &= ~CLK_EN;
	
//	tlc5955_internal_init();
}

void tlc5955_clear_array( void )
{
	unsigned int i;
	for( i = 0 ; i < 48 ; i++ )
	{
		tlc_dma_array[i] = 0;
	}
}

/* setupt internal registers of tlc5955 */
void tlc5955_internal_init( void )
{

	tlc5955_clear_array();



	//tlc_dma_array[47] = 0x9600;

#define DSPRPT 1
#define TMGRST 2
#define RFRESH 4
#define ESPWM 8
#define LSDVLT 16

	/* fill the 371 bit control data latch */
	/* Set 767:760 to 0x96 to enable control data update */
	tlc5955_byte_insert( tlc_dma_array, 8, 0x96, 760 );


	/* setup data  [Type]:[bits]*/
	/* 370 >|- FC:5 -|- BC:21 -|- MC:9 -|- DC:336 -|< 0 */
	/* Frunction */
	tlc5955_byte_insert( tlc_dma_array, 5, (DSPRPT|ESPWM|RFRESH), 366 );

	/* Brightness Control [R,G,B] */
	tlc5955_byte_insert( tlc_dma_array, 7, 0x0F, 345 ); /* Red */
	tlc5955_byte_insert( tlc_dma_array, 7, 0x02, 352 ); /* Green */
	tlc5955_byte_insert( tlc_dma_array, 7, 0x01, 359 ); /* Blue */

	/* Max current Control [R,G,B] */
	tlc5955_byte_insert( tlc_dma_array, 3, 0x06, 336 ); /* Red */
	tlc5955_byte_insert( tlc_dma_array, 3, 0x04, 339 ); /* Green */
	tlc5955_byte_insert( tlc_dma_array, 3, 0x04, 342 ); /* Blue */

	/* Dot Correction */
	unsigned int i;
	for( i = 0 ; i < 48 ; i++ )
	{
	//	tlc5955_byte_insert( tlc_dma_array, 7, 0x00, i*7 ); 
	}

	


	/* enable latch to send data into control data latch */
	tlc5955_xmit_latch_bit( true );

	/* Transmit 768 extra bits */
	tlc5955_xmit_common();

	
	tlc5955_toggle_latch();

	/* enable latch to send data into control data latch */
	tlc5955_xmit_latch_bit( true );

	/* Transmit 768 extra bits */
	tlc5955_xmit_common();


	tlc5955_toggle_latch();



}

/* set pixel i, to [r,g,b] value */
void tlc5955_set_pixel(  unsigned int idx, unsigned int r,  unsigned int g,  unsigned int b )
{
  unsigned int* ptr = &tlc_dma_array[(idx*3)];
  ptr[0] = r;
  ptr[1] = g;
  ptr[2] = b;
}

/* Enable GSCLK oscilator */
void tlc5955_enable_gsclk( void )
{
	P6OUT |= CLK_EN;
}

/* Disable GSCLK oscilator */
void tlc5955_disable_gsclk( void )
{
	P6OUT &= ~CLK_EN;
}

void tlc5955_toggle_latch( void )
{
	unsigned char tmp = P6OUT;
//	tlc5955_disable_gsclk();

	/* pulse latch */
	P3OUT |= LAT;
	P3OUT &= ~LAT;

	P6OUT = tmp;
}


};
