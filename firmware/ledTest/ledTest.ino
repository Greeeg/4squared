#include "tlc5955.h"
#include <stdbool.h>


void start_usb_boot_loader(void)
{
    typedef void (*functionpointer)(void);
    const functionpointer bsloader = (functionpointer)(0x1000);
    //USB_disconnect();
    //USB_disable();
    SYSBSLC &= ~(SYSBSLPE);
    __disable_interrupt();
    bsloader();          // won't return
}


void setup()
{

	
	//WDTCTL = WDTPW | WDTHOLD;
	// put your setup code here, to run once:
	tlc5955_port_init();


	tlc5955_internal_init();

	tlc5955_enable_gsclk();
	
 
}

void loop()
{

	PAREN = 0xFFFF;
	PAOUT = 0xFFFF;
	PADIR = 0;

	if( PAIN != 0xFFFF)
		start_usb_boot_loader();

// put your main code here, to run repeatedly:
		
		tlc5955_clear_array();

	unsigned int j;
	

	unsigned int i = 0; 


	while(1)
	{


		
		unsigned int in = PAIN;

		if(in != 0xFFFF)
			tlc5955_clear_array();
/*
		while(i < 15)
		{
			
			if(in & 1)
			{
				tlc5955_set_pixel(i,0,0,0);
			}
			else
				tlc5955_set_pixel(i,-1,-1,-1);
			
			i++;

			in >>= 1;

		}
		*/

		if(i++ > 15)
			i = 0;
			
		    
			tlc5955_set_pixel(i, rand(),rand()/3, rand()/5);
		


			tlc5955_xmit_latch_bit( false );
			tlc5955_xmit_common();
			tlc5955_toggle_latch();

			de();


		

			
		
	}
}


void de()
{

	unsigned int a,b;
	a = -1;
	b =  3;
	while( b--)
		while(a--)
			__asm__("nop \n");

}