#ifndef TLC5955_H__
#define TLC5955_H__


extern "C" {


void dma_init( void );

/* Transfer 96 bytes, 768 bits */
void tlc5955_xmit_common( void );

/* transfer latch select bit */
void tlc5955_xmit_latch_bit( bool );

/* insert bit stream segment, of [width] from [input_data] into main bitstream [array] at [offset] */
void tlc5955_byte_insert( unsigned int* array, unsigned int width, unsigned int input_data, unsigned int bit_offset );

/* initilise MSP430 end of communications */
void tlc5955_port_init( void );

/* setupt internal registers of tlc5955 */
void tlc5955_internal_init( void );

/* set pixel i, to [r,g,b] value */
void tlc5955_set_pixel(  unsigned int idx, unsigned int r,  unsigned int g,  unsigned int b );

/* Enable GSCLK oscilator */
void tlc5955_enable_gsclk( void );

/* Disable GSCLK oscilator */
void tlc5955_disable_gsclk( void );

void tlc5955_clear_array( void );

void tlc5955_toggle_latch( void );

/* static 38 int array, 768 bits */
extern unsigned int tlc_dma_array[48];


};

#endif